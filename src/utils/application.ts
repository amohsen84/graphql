import * as path from "path";
import { NextFunction, Request, Response, Router } from "express";
import { GraphQLServer, PubSub } from "graphql-yoga";
import { fileLoader, mergeTypes } from "merge-graphql-schemas";
import { HttpError, InternalServerError, NotFound } from "http-errors";
import { logHTTP } from "../middleware/logger.middleware";
import { Logger } from "./logger";
import {resolvers, permissions} from "../resolvers";

const logger = Logger.getInstance();

/**
 *  The main API application instance
 *  Here middleware are assigned 
 *  Also handlers are assigned for logging and returning errors
 * @class  Application
 */
export class Application {
    
    /**
     * Get/create a singleton GraphQL Application instance
     */
    public static getInstance(): GraphQLServer {
        // Check whether the instance already created or not
        if (this.instance) {
            return this.instance;
        }
        // Merge schemas
        const typesArray = fileLoader(path.join(__dirname, "../schemas"));
        const typeDefs = mergeTypes(typesArray, { all: true });
        // Create a pubsub instance (for web socket handling, we use it 
        // to push notification to subscribed channels)
        const pubsub = new PubSub();
        // Create application instance
        this.instance = new GraphQLServer({
            typeDefs,
            resolvers,
            context(request) {
                return {
                    pubsub,
                    request
                };
            },
            middlewares: [permissions]
        });
        // Log request/response middleware
        this.instance.express.use(logHTTP);
        // Error handler
        // This catches any error thrown in the aplication
        // If the error is an HttpError, it is used in the response
        // For all other errors, the error is logged and an Internal Server Error is returned
        this.instance.express.use((err: HttpError | Error, req: Request, res: Response, next: NextFunction) => {
            if (err instanceof HttpError) {
                // Respond with thrown HTTP Errors
                res.status(err.statusCode);
                res.jsonp({ error: { message: err.message } });
            } else {
                // Log other Errors and respond with Internal Server Error
                logger.error(err);
                const ise = new InternalServerError();
                res.status(ise.statusCode);
                res.jsonp({ message: ise.message });
            }
        });
        return this.instance;
    }
    /** GraphQL application singleton instance */
    private static instance: GraphQLServer;
}
