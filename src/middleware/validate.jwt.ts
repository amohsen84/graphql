import * as jwt from "jsonwebtoken";
import { Logger } from "../utils/logger";

// Get an instance of the logger
const logger = Logger.getInstance();

/**
 * Validate JWT Token passed on the header
 */
export function validateJWT(resolve, parent, args, {request}, info)  {
    logger.info("Validating JWT");
    // Read the authoriztion field from the header
    const header = request.request ? request.request.headers.authorization : request.connection.context.Authorization;
    if (header) {
        // Get the JWT
        const token = header.replace("Bearer ", "");
        // Decode the passed JWT using the secret
        // It will throw an error if it fails to decode with the secret
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        logger.info("Successful JWT Validation", decoded);
        return resolve();
    } else { 
        throw new Error("JWT not found in the request.");
    }
}
