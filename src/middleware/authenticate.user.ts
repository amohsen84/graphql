import { NextFunction, Request, Response, Router } from "express";
import { Logger } from "../utils/logger";

const logger = Logger.getInstance();

/**
 * Authenticate User
 * This is a dummy function as this is a mock-up code (for demo purposes)
 * In reality, this function should  check J-Token passed with the request.
 * 
 *
 * @param req Express Request
 * @param res Express Response
 * @param next Express Next Function
 */
export function authenticateUser(req: Request, res: Response, next: NextFunction) {
    // Add authentication code here
    logger.info("Authentication Phase");
    next();
}
