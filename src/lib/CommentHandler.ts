import data from "../data";
import {IComment, IPost, IUser} from "../interfaces";
export class CommentHandler {
    public static getCommentDetails(commentId: string): IComment {
        return data.comments.find((comment) => {
            return comment.id === commentId;
        });
    }
    public static getAllComments(): IComment[] {
        return data.comments;
    }
}
