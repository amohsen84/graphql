import data from "../data";
import {IComment, IPost, IUser} from "../interfaces";
export class PostHandler {

    public  static getPostDetails(postId: string): IPost {
        return data.posts.find((post) => {
            return post.id === postId;
        });
    }
    public static getCommentsByPost(postId: string): IComment[] {
        return data.comments.filter((comment) => {
            return comment.postId === postId;
        });
    }
    public static getAllPosts(): IPost[] {
        return data.posts;
    }

}
