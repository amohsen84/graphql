import uuidv4 from "uuid/v4";
import data from "../data";
import {IComment, IPost, IUser} from "../interfaces";

export class UserHandler {

    public static getCommentsByAuthor(authorId: string): IComment[] {
        return data.comments.filter((comment) => {
            return comment.authorId === authorId;
        });
    }

    public  static getPostsByAuthor(authorId: string): IPost[] {
        return data.posts.filter((post) => {
            return post.authorId === authorId;
        });
    }

    public  static getUser(userId: string): IUser {
        return data.users.find((user) => {
            return user.id === userId;
        });
    }

    public  static getAllUsers(): IUser[] {
        return data.users;
    }

}
