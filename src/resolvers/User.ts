import { DataHandler } from "../lib/data.handler";
import { EntityType } from "../enum/EntityType";
/**
 * User's fields with custom types
 */
const User = {
    /*
     * Get all posts created by the specified author
     */
    posts(parent, args, cxt, info) {
        return DataHandler.getItems(EntityType.Post, {authorId: parent.id});
    },
    /*
     * Get all comments created by the specified author
     */
    comments(parent, args, cxt, info) {
        return DataHandler.getItems(EntityType.Comment, {authorId: parent.id});
    }
};

export { User as default };
