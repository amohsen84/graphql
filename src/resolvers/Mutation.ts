import { DataHandler } from "../lib/data.handler";
import { EntityType } from "../enum/EntityType";
import {authenticateUser} from "../lib/authenticate.user";

/**
 * Mutation Operations
 */  
const Mutation = {
    /**
     * login 
     */  
    login(parent, args, cxt, info): string {
        return authenticateUser(args.login, args.pwd);
    },
    /**
     * Async activities load
     */  
    asyncActivitiesLoad(parent, args, { pubsub }, info) {
        // We are getting the data inside the timeout callback to provide an example 
        // of pushing the data through the websocket after returning a response  
        setTimeout(() => {
            // Get all posts created by the specified author.
            const posts = DataHandler.getItems(EntityType.Post, args);
            // Get all comments created by the specified author.
            const comments = DataHandler.getItems(EntityType.Comment, args);
            // publish the data to the user activities channel (Channels are created by a subscription)
            pubsub.publish(`userActivities ${args.authorId}`, {
                userActivitiesLoaded: {
                    posts,
                    comments
                }
            });
        }, 2000);
        return true;
    }
};

export { Mutation as default };
