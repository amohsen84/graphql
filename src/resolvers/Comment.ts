import { DataHandler } from "../lib/data.handler";
import { EntityType } from "../enum/EntityType";
import {IPost, IUser} from "../interfaces";

/**
 * Comment's fields having custom types 
 */
const Comment = {
    /**
     * Comment's author 
     */  
    author(parent, args, cxt, info): IUser {
        const items = DataHandler.getItems(EntityType.User, {
            id: parent.authorId 
        });
        if(!items.length) {
            throw new Error("Invalid comment as it is not associated with a user.");
        }
        return items[0] as IUser;
    },
    /**
     * Comment's post
     */  
    post(parent, args, cxt, info): IPost {
        const items = DataHandler.getItems(EntityType.Post, {
            id: parent.postId 
        });
        if(!items.length) {
            throw new Error("Invalid comment as it is not associated with a post.");
        }
        return items[0] as IPost;
    }
};

export { Comment as default };
