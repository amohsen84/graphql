import { DataHandler } from "../lib/data.handler";
import { EntityType } from "../enum/EntityType";
/**
 * Queries
 */
const Query = {
    /**
     * Get users matching the search criteria specified in the args parameter
     */
    users(parent, args, cxt, info) {
        return DataHandler.getItems(EntityType.User, args);
    },
    /**
     * Get posts matching the search criteria specified in the args parameter
     */
    posts(parent, args, cxt, info) {
        return DataHandler.getItems(EntityType.Post, args);
    },
    /**
     * Get comments matching the search criteria specified in the args parameter
     */
    comments(parent, args, cxt, info) {
        return DataHandler.getItems(EntityType.User, args);
    }
};
export { Query as default };
