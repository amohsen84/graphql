import { DataHandler } from "../lib/data.handler";
import { EntityType } from "../enum/EntityType";
import {IComment, IUser} from "../interfaces";

/**
 * Post's fields having custom types 
 */
const Post = {
    /**
     * Post's author 
     */  
    author(parent, args, cxt, info): IUser {
        const items = DataHandler.getItems(EntityType.User, {
            id: parent.authorId 
        });
        if(!items.length) {
            throw new Error("Invalid comment as it is not associated with a user.");
        }
        return items[0] as IUser; 
    },
    /**
     * Comments linked to the post
     */  
    comments(parent, args, cxt, info): IComment[] {
        return DataHandler.getItems(EntityType.Comment, {
            postId: parent.id
        }) as IComment[];
    }
};

export { Post as default };
