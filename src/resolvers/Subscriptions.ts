
/**
 * Subscription Operations
 */  
const Subscription = {
    /**
     * Subscribe to user activities channel
     * the channel will be updated when all activies of the specified author  are fully loaded
     */  
    userActivitiesLoaded: {
        subscribe(parent, args, { pubsub }, info) {
            return pubsub.asyncIterator(`userActivities ${args.authorId}`);
        }
    }
};

export { Subscription as default };
