/**
 * This interface represents the user entity.
 * @interface IUser
 */
export interface IUser {
    id: string;
    name: string;
    email: string;
    age: number;
}
