/**
 * This interface represents the post entity.
 * @interface IPost
 */
export interface IPost {
    id: string;
    title: string;
    body: string;
    published: boolean;
    authorId: string;
}
