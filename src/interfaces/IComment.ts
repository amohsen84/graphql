/**
 * This interface represents the comment entity.
 * @interface IComment
 */
export interface IComment {
    id: string;
    text: string;
    authorId: string;
    postId: string;
}
