GraphQL Boilerplate

Overview
=======================
    - This code base is a template that provides the foundation for building a service based on GraphQL.


Technologies Used
-----------------
    - The service has been written in typescript.
    - The GraphQL Yoga Framework is used to facilitate implementing the service based on GraphQL.
    - JEST is used for running tests.


Installation
------------
 Run the following commands to install dependencies:

    > cd {Project-Directory}
    > npm install


Build
------
Run the following commands to transpile the code:

    > cd {Project Directory}
    > npm run build


Rnning the service
------------------
Run the following commands to run the service:

    > cd {Project Directory}
    > npm start


Auto Test
----------
Run the following commands to run the tests:

    > cd {Project Directory}
    > npm run test

In addition to the outcome of the tests, a coverage document is generated highlighting lines covered by the tests.


Project Structure
-----------------
    /src
      /interfaces    # Interfaces definitions for entities used in the service.
      /lib           # Includes classes in charge of data handling (e.g. fetching data and authentication).
      /middleware    # Middleware used by GraphQL operations (e.g. validate auth token).
      /schemas       # GraphQL schemas.
      /resolvers     # Implementation of queries and operations defined in GraphQL schemas.
      /tests         # Test cases definitions.
      /utils         # Tools that 
    /config          # Configurations based on the selected environment


The playground tool
-----------------------------------
The GraphQL Yoga provides a tool called GraphQL playground that helps in testing the service from your browser. In addition, it includes a documentation for all types, queries, mutation operations, and subscriptions defined in the service. You need to run the service first then hits the following URL in your browser:

    https://localhost:4000
